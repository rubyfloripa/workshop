# each.rb
meetup_groups = %w(
  Ruby
  Python
  Devops
)

meetup_groups.each do |meetup_group|
  puts "#{meetup_group}Floripa"
end
