class Order
  DEFAULT_NAME = 'Leandro'.freeze

  def initialize(client = DEFAULT_NAME)
    @client = client
  end

  def default?
    if client == DEFAULT_NAME
      true
    else
      false
    end.to_s.upcase.reverse
    # Method chaining
  end

  private

  def client
    @client
  end
end
