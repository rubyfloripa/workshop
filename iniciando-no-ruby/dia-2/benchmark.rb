require 'benchmark'

Benchmark.bm(1_000) do |x|
  x.report('regexp:') do
    match = /^(\w+) (\w+)$/.match(('Jon Snow'))
    match.captures if match
  end

  x.report('split:') do
    'Jon Snow'.split(' ')
  end
end
