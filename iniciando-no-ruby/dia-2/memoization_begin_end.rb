# memoization_begin_end.rb
class Event
  def attendees
    @attendees ||= begin
                     # long duration processing
                     duration = rand(3..5)
                     puts "Processing in #{duration} secs"
                     sleep(duration)

                     16
                   end
  end
end

event = Event.new
p "#{Time.now} - #{event.attendees}"
p "#{Time.now} - #{event.attendees}"
