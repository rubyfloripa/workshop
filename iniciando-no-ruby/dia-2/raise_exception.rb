# raise_exception.rb
class LastNameValidator
  def self.valid?(value)
    if value.include?(' ')
      raise StandardError
    end

    true
  end
end

p LastNameValidator.valid?('Ruby')
p LastNameValidator.valid?('Ruby Floripa')
