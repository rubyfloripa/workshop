# rescue_exception.rb
class FileViewer
  def self.view(filepath)
    begin
      File.open(filepath).read
    rescue Errno::ENOENT
      puts 'File not found'
    end
  end
end

FileViewer.view('/tmp/rubyfloripa.txt')
