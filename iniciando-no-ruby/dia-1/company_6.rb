# company_6.rb
class Company
  def register_employees!
    @employees = ['Elon Musk', 'Tom Mueller']
  end

  def employees
    @employees
  end
end

space_x = Company.new
puts "Employees: #{space_x.employees}"
space_x.register_employees!
puts "Employees: #{space_x.employees}"
