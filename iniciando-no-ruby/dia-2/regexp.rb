require 'benchmark'

class Person
  VALID_NAME = /^(\w+) (\w+)$/

  def self.split_with_regexp(full_name)
    match = VALID_NAME.match(full_name)

    match.captures if match
  end

  def self.split(full_name)
    full_name.split(' ')
  end
end

Benchmark.bm(100) do |x|
  x.report('regexp:') do
    p Person.split_with_regexp('Jon Snow')
  end

  x.report('split:') { p Person.split('Jon Snow') }
end
