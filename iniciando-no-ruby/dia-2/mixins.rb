# mixins.rb
module Vision
  def see
    puts 'I can see'
  end
end

module Nose
  def smell
    puts 'I can smell'
  end
end

class Person
  include Vision
  include Nose
end

person = Person.new
person.see
person.smell
