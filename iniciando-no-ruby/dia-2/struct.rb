# struct.rb
Event = Struct.new(:name, :attendees) do
  def ordered_attendees
    attendees.sort
  end
end

day_1 = Event.new('Workshop \'Iniciando no Ruby\' - Dia 1', [
 'Person A',
 'Person C',
 'Person B',
])

p day_1.name
p day_1.attendees
p day_1.ordered_attendees
