# time.rb
workshop_date = Time.new(2018, 07, 07, 8, 30)

puts "Ano: #{workshop_date.year}"
puts "Mês: #{workshop_date.month}"
puts "Dia: #{workshop_date.day}"
puts "Horas: #{workshop_date.hour}"
puts "Minutos: #{workshop_date.min}"
puts "Segundos: #{workshop_date.sec}"

puts "Sábado? #{workshop_date.saturday?}"
seconds_since_epoch = workshop_date.to_i
puts Time.at(seconds_since_epoch)
