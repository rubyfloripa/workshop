# custom_exception.rb
class MyStorage
  class InvalidSlot < StandardError; end;

  AVAILABLE_SLOTS = 1..100

  def self.store!(slot)
    unless AVAILABLE_SLOTS.include?(slot)
      raise InvalidSlot
    end

    'Success!'
  end
end

p MyStorage.store!(1)
p MyStorage.store!(-10)
