# inheritance.rb
class User
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def greeting
    "Olá! - Disse #{name}"
  end
end

class Admin < User
end

puts User.new('Participante').greeting
puts Admin.new('RubyFloripa Admin').greeting
