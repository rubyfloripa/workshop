# if_elsif_else.rb
def fizz_buzz(number)
  result = ''

  if (number % 3).zero? && (number % 5).zero?
    result = 'FizzBuzz'
  elsif (number % 3).zero?
    result = 'Fizz'
  elsif (number % 5).zero?
    result = 'Buzz'
  end

  result.empty? ? number : result
end

puts fizz_buzz(3)
puts fizz_buzz(5)
puts fizz_buzz(15)
puts fizz_buzz(7)
