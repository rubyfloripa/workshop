# switch_case.rb
class DrinkMachine
  def self.generate(temperature)
    case temperature
    when 14..17
      'Café'
    when 0..13
      'Chocolate quente'
    when 18..32
      'Suco de frutas'
    else
      'Água'
    end
  end
end

puts DrinkMachine.generate(13)
puts DrinkMachine.generate(14)
puts DrinkMachine.generate(30)
puts DrinkMachine.generate(-1)
