# date.rb
require 'date'

date = Date.new(2018, 1, 20)
puts "Dia: #{date.day}"
puts "Mês: #{date.month}"
puts "Ano: #{date.year}"

new_date = Date.parse('2018-03-23')
puts "----\nUsing parse:"
puts "Dia: #{new_date.day}"
puts "Mês: #{new_date.month}"
puts "Ano: #{new_date.year}"

new_date_with_format = Date.strptime('13-08-2018', '%d-%m-%Y')
puts new_date_with_format.inspect

date_from_time = Time.new(2018, 10, 23).to_date
puts date_from_time.inspect
