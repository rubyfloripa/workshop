class Company
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  private

  def happening?
    true
  end

  # Returns company quantity from database
  def self.count
    10
  end
  private_class_method :count
end
