# modules.rb
module Fish
  module Tainha
    def self.quantity
      40 * rand(3..5)
    end
  end
end

module Fish
  module Tuna
    def self.quantity
      30 * rand(2..10)
    end
  end
end

p Fish::Tainha.quantity
p Fish::Tuna.quantity
