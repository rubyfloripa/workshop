# map.rb
Company = Struct.new(:name)

companies = [
  Company.new('RubyFloripa'),
  Company.new('New Company'),
  Company.new('XYZ Company'),
]

company_names = companies.map do |company|
  company.name
end

puts company_names.inspect
