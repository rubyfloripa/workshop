# class_variables.rb
class Person
  @@default_max_beer_cups = 2

  def self.default_max_beer_cups
    @@default_max_beer_cups
  end
end

class Manezinho < Person
  @@default_max_beer_cups = 5
end

puts Manezinho.default_max_beer_cups # 5
puts Person.default_max_beer_cups # 5, oops!!
