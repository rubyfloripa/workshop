# times_and_upto.rb
10.times do |n|
  result = (n % 2).zero? ? 'Par' : 'Ímpar'

  puts "Número #{n} é #{result}"
end

puts 4.upto(10).to_a.inspect
