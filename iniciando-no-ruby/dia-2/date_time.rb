# date_time.rb
require 'date'

date = DateTime.new(2018, 1, 20, 13, 40, 50)
puts "Dia: #{date.day}"
puts "Mês: #{date.month}"
puts "Ano: #{date.year}"
puts "Hora: #{date.hour}"
puts "Minuto: #{date.minute}"
puts "Segundo: #{date.second}"

new_date = DateTime.parse('2018-03-23 18:35:10')
puts new_date.inspect
