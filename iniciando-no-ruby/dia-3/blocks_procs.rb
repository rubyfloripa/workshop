# blocks_procs.rb
[1, 2, 3].each do |number|
  puts number
end
[1, 2, 3].each { |number| p number }
def some_method(arg1, arg2)
  p arg1
  p arg2
  yield
end

some_method('one', 'two') do
  p 'three is being printed by the block'
end

my_proc = proc { |block_arg| p block_arg.upcase }
my_proc.call 'rubyfloripa'
a_proc = proc { |scalar, *values| values.map { |value| p value * scalar } }
a_proc.yield(23, 4, 16, 12)
